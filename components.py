import numpy as np
import numpy.linalg as LA

class Component():
	def __init__(self, pos, type, pins):
		self.sch_pos = pos
		self.brd_pos = (0,0)
		self.type = type
		self.tags = set()
		self.pins_offset_sch = [(-24, 0), (24, 0)]
		self.pins_offset_brd = [(-24, 0), (24, 0)]
		self.connections = [[] for i in range(pins)]
		self.orientation_sch = 0
		self.orientation_brd = 0

	def rotate(self, layout):
		if layout == "sch":
			self.orientation_sch = (self.orientation_sch+1)%4
			self.move(self.sch_pos, layout)	# update wire endpoints
		else:
			self.orientation_brd = (self.orientation_brd+1)%4

	def get_orientation(self, layout):
		if layout == "sch":
			return self.orientation_sch
		else:
			return self.orientation_brd

	def set_pos(self, pos, layout):
		if layout == "sch":
			self.sch_pos = pos
		elif layout == "brd":
			self.brd_pos = pos

	def get_pos(self, layout):
		if layout == "sch":
			return self.sch_pos
		elif layout == "brd":
			return self.brd_pos

	def get_pins(self, layout):
		if layout == "sch":
			return [tuple(np.add(self.sch_pos, self.get_pin_offset(0, layout))),
					tuple(np.add(self.sch_pos, self.get_pin_offset(1, layout)))]
		else:
			return [tuple(np.add(self.brd_pos, self.get_pin_offset(0, layout))),
					tuple(np.add(self.brd_pos, self.get_pin_offset(1, layout)))]

	def get_close_pin(self, pos, dist, layout):
		if layout == "sch":
			pin1 = self.get_pin_pos(0, "sch")
			pin2 = self.get_pin_pos(1, "sch")
		else:
			pin1 = self.get_pin_pos(0, "brd")
			pin2 = self.get_pin_pos(1, "brd")
		pin1_d = LA.norm(np.subtract(pos, pin1))
		pin2_d = LA.norm(np.subtract(pos, pin2))
		pin = None
		pin_pos = pos
		min_dist = dist
		if pin1_d < min_dist:
			pin = 0
			pin_pos = pin1
			min_dist = pin1_d
		if pin2_d < min_dist:
			pin = 1
			pin_pos = pin2
			min_dist = pin2_d
		return pin, pin_pos, min_dist

	def get_pin_pos(self, pin, layout):
		return self.get_pins(layout)[pin]

	def move(self, pos, layout):
		if layout == "sch":
			self.sch_pos = pos
			for pi, pin in enumerate(self.connections):
				for w, p in pin:
					w.move_endpoint(p, self.get_pins(layout)[pi])
		else:
			self.brd_pos = pos

	def move_pin(self, pin, pos, layout):
		if layout == "brd":
			orient = self.orientation_brd
			offset = self.pins_offset_brd
			move = tuple(np.subtract(pos, self.brd_pos))
			if orient == 0:
				self.pins_offset_brd[pin] = move
			elif orient == 1:
				self.pins_offset_brd[pin] = (-move[1], move[0])
			elif orient == 2:
				self.pins_offset_brd[pin] = (-move[0], -move[1])
			elif orient == 3:
				self.pins_offset_brd[pin] = (move[1], -move[0])
		else:
			exit()

	def get_pin_offset(self, pin, layout):
		if layout == "sch":
			orient = self.orientation_sch
			offset = self.pins_offset_sch
		else:
			orient = self.orientation_brd
			offset = self.pins_offset_brd

		if orient == 0:
			return offset[pin]
		elif orient == 1:
			return (offset[pin][1], -offset[pin][0])
		elif orient == 2:
			return (-offset[pin][0], -offset[pin][1])
		elif orient == 3:
			return (-offset[pin][1], offset[pin][0])

	def add_tag(self, tag):
		self.tags.add(tag)

	def remove_tag(self, tag):
		self.tags.remove(tag)

	def has_tag(self, tag):
		return tag in self.tags

	def get_connections(self):
		return self.connections

	def get_connections(self, pin):
		return self.connections[pin]

	def add_connection(self, pin, other):
		self.connections[pin].append(other)

	def remove_connection(self, pin, other):
		self.connections[pin].remove(other)
	
	def delete(self):
		for pi, pin in enumerate(self.connections):
			for c, p in pin:
				RemoveConnection(c, p, self, pi)

class Wire(Component):
	def __init__(self):
		Component.__init__(self, (0,0), "wire", 2)
		self.coords = []

	def add_point(self, point):
		self.coords.append(point)

	def get_points(self):
		return self.coords

	def get_pins(self):
		return [self.coords[0], self.coords[-1]]

	def get_pin_pos(self, pin, layout):
		if layout == "sch":
			if pin == 0:
				return self.coords[0]
			else:
				return self.coords[-1]
		elif layout == "brd":
			exit()

	def move_endpoint(self, pin, pos):
		if pin == 0:
			self.coords[0] = pos
		if pin == 1:
			self.coords[-1] = pos

	def move_segment(self, p0, pos):
		if ((p0 != 0 and p0+1 != len(self.coords)-1)
			or (p0 == 0 and len(self.connections[0]) == 0)
			or (p0+1 == len(self.coords)-1 and len(self.connections[1]) == 0)):

			p0_pos = self.coords[p0]
			p1_pos = self.coords[(p0+1)]
			p_off = np.divide(np.subtract(p1_pos, p0_pos), 2)
			new_p0 = np.subtract(pos, p_off)
			new_p1 = np.add(pos, p_off)

			self.coords[p0] = tuple(new_p0)
			self.coords[(p0+1)] = tuple(new_p1)
		elif (p0 == 0 and len(self.connections[0]) != 0):
			self.move_point(p0+1, pos)
		elif (p0+1 == len(self.coords)-1 and len(self.connections[1]) != 0):
			self.move_point(p0, pos)

	def set_point_pos(self, pi, pos):
		self.coords[pi] = pos

	def move_point(self, pi, pos):
		move = True
		if pi == 0:
			for c, p in self.connections[0]:
				if c.type != "wire":
					move = False
					break
			if move == True:
				self.coords[pi] = pos
				for c, p in self.connections[0]:	#move all connected wires
					c.set_point_pos(p, pos)
		elif pi == len(self.coords) -1:
			for c, p in self.connections[1]:
				if c.type != "wire":
					move = False
					break
			if move == True:
				self.coords[pi] = pos
				for c, p in self.connections[1]:	#move all connected wires
					c.set_point_pos(p, pos)
		else:
			self.coords[pi] = pos

	def delete_segment(self, p0):
		new_wires = []
		if len(self.coords) == 2:
			for i, pc in enumerate(self.connections):
				for c, p in pc:
					RemoveConnection(c, p, self, i)
		else:
			new_wires.append(self)
			new_wire = None
			if len(self.coords)-1 > p0+1:
				new_wire = Wire()
				new_wire.coords = self.coords[(p0+1):]
				new_wires.append(new_wire)

			for c in self.connections[1]:			#remove connections from other end
				RemoveConnection(c[0], c[1], self, 1)
				if new_wire is not None:
					AddConnection(c[0], c[1], new_wire, 1)
			self.coords = self.coords[:(p0+1)]

		return new_wires


def AddConnection(object1, pin1, object2, pin2):
	object1.add_connection(pin1, (object2, pin2))
	object2.add_connection(pin2, (object1, pin1))

def RemoveConnection(object1, pin1, object2, pin2):
	object1.remove_connection(pin1, (object2, pin2))
	object2.remove_connection(pin2, (object1, pin1))
