from tkinter import filedialog
from tkinter import *
import numpy as np
import numpy.linalg as LA
import pickle

from components import Component, Wire, AddConnection, RemoveConnection
from pcb import Pcb

class Window(Frame):
	def __init__(self, master=None):
		Frame.__init__(self, master)
		self.master = master
		self.master.title("Schematic")

		self.master.bind("<Control-s>", self.save)
		self.master.bind("<Control-o>", self.load)
		self.master.bind("<Key>", self.key)
		self.pack(fill="both", expand=True)

		self.load_assets()
		self.init_menu()
		self.init_sidebar()
		self.init_drawing_area()

		self.mouse_p = (0, 0)

		self.grid = 12
		self.strip = False
		self.current_layout = "sch"
		self.current_mode = ""
		self.current_wire = None
		self.current_component = None
		self.wires = []
		self.components = []

		self.pcb = Pcb()
		self.pcb.update_stripboard()

	def load_assets(self):
		self.r_image_h = PhotoImage(file = "Assets\SCH\R_h.png")
		self.r_image_v = PhotoImage(file = "Assets\SCH\R_v.png")
		self.r_image_brd_h = PhotoImage(file = "Assets\BRD\R_h.png")
		self.r_image_brd_v = PhotoImage(file = "Assets\BRD\R_v.png")
		# self.c_image = PhotoImage(file = "Assets\SCH\C.png")
		self.sch_image = PhotoImage(file = "Assets\SCH.png")
		self.brd_image = PhotoImage(file = "Assets\BRD.png")

	def init_menu(self):
		menu = Menu(self.master)
		self.master.config(menu=menu)
		file = Menu(menu)
		file.add_command(label="Exit", command=self.client_exit)
		menu.add_cascade(label="File", menu=file)

	def init_sidebar(self):
		self.component_frame = Frame(self, bg="grey")
		self.component_frame.pack(side="left", fill="both")
		self.layout_mode_button_sch = Button(self.component_frame, image=self.sch_image, command=self.draw_layout_sch_b).pack(side="top", fill="x")
		self.layout_mode_button_brd = Button(self.component_frame, image=self.brd_image, command=self.draw_layout_brd_b).pack(side="top", fill="x")
		self.r_button = Button(self.component_frame, image=self.r_image_h, command=self.place_resistor).pack(side="top", fill="x")

	def init_drawing_area(self):
		self.canvas = Canvas(self, bg="black")
		self.canvas.bind("<Button-1>", self.mouse1)
		self.canvas.bind("<Button-3>", self.mouse3)
		self.canvas.bind("<Motion>", self.mouse_move)
		self.canvas.pack(side="left", fill="both", expand=True)

	def client_exit(self):
		exit()

	def draw_layout_sch_b(self):
		self.current_mode = ""
		self.current_wire = None
		self.current_component = None
		self.draw_layout("sch")

	def draw_layout_brd_b(self):
		self.current_mode = ""
		self.current_wire = None
		self.current_component = None
		self.draw_layout("brd")

	def draw_layout(self, layout):
		self.current_layout = layout
		if layout == "sch":
			self.draw_layout_sch()
		else:
			self.draw_layout_brd()

	def draw_layout_sch(self):
		print("Mode: sch")
		self.canvas.delete("all")
		self.strip = False
		for c in self.components:
			if c.type == "resistor":
				if c.get_orientation(self.current_layout)%2 == 0:
					img = self.r_image_h
				else:
					img = self.r_image_v
				self.canvas.create_image(c.get_pos("sch"), image=img, tags=("component", "resistor"))
		for w in self.wires:
			points = w.get_points()
			for (s,e) in zip(points[:-1], points[1:]):
				self.canvas.create_line(s[0], s[1], e[0], e[1], fill="green", tags="wire")

	def draw_layout_brd(self):
		print("Mode: brd")
		self.canvas.delete("component")
		self.canvas.delete("signal")
		self.canvas.delete("wire")
		self.canvas.delete("indicator")
		if not self.strip:
			self.pcb.draw_stripboard(self.canvas)

		if self.pcb.changed:
			self.pcb.create_nets(self.components)

		ind_d = 3
		for i, c in enumerate(self.components):
			p = c.get_pos("brd")
			if p == (0,0):
				c.set_pos((60, 60+60*i), "brd")
			if c.get_orientation(self.current_layout)%2 == 0:
				img = self.r_image_brd_h
			else:
				img = self.r_image_brd_v
			self.canvas.create_image(c.get_pos("brd"), image=img, tags=("component"))
			for pin in c.get_pins("brd"):
				self.canvas.create_oval((pin[0]-ind_d, pin[1]-ind_d, pin[0]+ind_d, pin[1]+ind_d), fill="red", tags=("indicator"))

		for n in self.pcb.get_nets():
			net = list(n)
			for i, (c0, p0) in enumerate(net):
				c0_p0_pos = c0.get_pin_pos(p0, "brd")
				min_dist = 100000
				conn = None
				for c1, p1 in net[(i+1):]:
					dist = LA.norm(np.subtract(c0_p0_pos, c1.get_pin_pos(p1, "brd")))
					if dist < min_dist:
						min_dist = dist
						conn = (c1, p1)
				if conn is not None:
					s = c0_p0_pos
					e = conn[0].get_pin_pos(conn[1], "brd")
					self.canvas.create_line(s[0], s[1], e[0], e[1], fill="yellow", tags="signal")


	def place_resistor(self):
		print("R")
		self.current_mode = "component"
		self.current_component = (Component(self.mouse_p, "resistor", 2), 0)
		self.components.append(self.current_component[0])
		self.draw_layout("sch")

	def place_wire(self):
		print("W")
		self.current_mode = "wire"

	def cancel(self):
		print("Esc")

	def move(self, layout):
		print("M")
		to_move = self.get_close_components(self.mouse_p, 15, layout)
		if len(to_move) > 0:
			self.current_mode = "move_c"
			self.current_component = to_move[0]
			return
		if layout == "sch":
			to_move = self.get_close_wire_point(self.mouse_p, 15)
			if len(to_move) > 0:
				self.current_mode = "move_w_point"
				self.current_component = to_move[0]
				return
			to_move = self.get_close_wire_segment(self.mouse_p, 15)
			if len(to_move) > 0:
				self.current_mode = "move_w_segment"
				self.current_component = to_move[0]
				return
		elif self.current_layout == "brd":
			c, p = self.get_closest_component_pin(self.mouse_p, 15, layout)
			if c is not None:
				self.current_mode = "move_brd_pin"
				self.current_component = (c, p)

	def delete(self, layout):
		print("Delete")
		changed = False
		to_delete_c = self.get_close_components(self.mouse_p, 15, layout)
		if len(to_delete_c) > 0:
			to_delete_c[0][0].delete()
			self.components.remove(to_delete_c[0][0])
			self.draw_layout(layout)
			changed = True
		else:
			to_delete_w = self.get_close_wire_segment(self.mouse_p, 15)
			if len(to_delete_w) > 0:
				new_wires = to_delete_w[0][0].delete_segment(to_delete_w[0][1])
				if len(new_wires) == 2:
					self.wires.append(new_wires[1])
				elif len(new_wires) == 0:
					self.wires.remove(to_delete_w[0][0])
				self.draw_layout(layout)
				changed = True

		if changed:
			self.pcb.clear()

	def key(self, event): 
		print("Symbol: " + event.keysym)
		if event.keysym == 'r':
			self.place_resistor()
		elif event.keysym == 'w':
			self.place_wire()
		elif event.keysym == 'm':
			self.move(self.current_layout)
		elif event.keysym == "Escape":
			self.cancel()
		elif event.keysym == "Delete" or event.keysym == "x":
			self.delete(self.current_layout)
		elif event.keysym == "q":
			self.client_exit()

	def mouse1(self, event):
		changed = False
		print("Click: ", event.x, event.y, event.widget)
		close = self.get_close_components((event.x, event.y), 45, self.current_layout)
		print("Close items: ", close)
		if self.current_mode[:4] == "move":
			if self.current_mode == "move_brd_pin":
				self.current_mode = ""
				if self.current_component[0].orientation_brd%2 ==0:
					pos = (self.mouse_p[0], self.current_component[0].brd_pos[1])
				else:
					pos = (self.current_component[0].brd_pos[0], self.mouse_p[1])
				self.current_component[0].move_pin(self.current_component[1], pos, self.current_layout)
				self.current_component = None
				self.draw_layout(self.current_layout)
			else:
				self.current_mode = ""
				self.current_component[0].set_pos(self.mouse_p, self.current_layout)
				self.current_component = None
				self.draw_layout(self.current_layout)

		elif self.current_layout == "sch":
			if self.current_mode == "wire":
				close.extend(self.get_close_wires_endpoint((event.x, event.y), 15))
				if self.current_wire == None:
					print ("created new wire")
					self.wires.append(Wire())
					self.current_wire = self.wires[-1]
					self.current_wire.add_tag("current")

				point = self.mouse_p
				min_dist = 15
				terminate = None
				pin = 0
				for c,c0 in close:
					cpin, point, min_dist = c.get_close_pin(self.mouse_p, min_dist, "sch")
					if cpin is not None:
						pin = cpin
						terminate = c

				self.current_wire.add_point(point)
				points = self.current_wire.get_points()
				if terminate is not None and len(points) > 1:
					AddConnection(terminate, pin, self.current_wire, 1)

					self.canvas.coords(self.current_line, points[-2][0], points[-2][1], point[0], point[1])
					self.current_wire.remove_tag("current")
					self.current_wire = None
					changed = True

				else:
					if terminate is not None:
						AddConnection(terminate, pin, self.current_wire, 0)
						changed = True
					self.current_line = self.canvas.create_line(0,0,0,0, fill="green", tags="wire")

			elif self.current_mode == "component":
					# self.components.append()
					# self.components.append(self.current_component[0])
					self.current_component = None
					self.current_mode = ""
					print(self.components)
					changed = True

		if changed:
			self.pcb.clear()

	def mouse3(self, event): 
		if self.current_mode == "wire" and self.current_wire != None:
			self.canvas.delete(self.current_line)
			self.current_wire.remove_tag("current")
			self.current_wire = None
		elif self.current_mode[:4] == "move" or self.current_mode == "component":
			if self.current_mode == "move_brd_pin":
				pass
			else:
				self.current_component[0].rotate(self.current_layout)
				self.draw_layout(self.current_layout)

	def mouse_move(self, event):
		self.mouse_p = self.snap_to_grid((event.x, event.y), self.current_layout)
		if self.current_mode == "wire" and self.current_wire != None:
			points = self.current_wire.get_points()
			if len(points) != 0:
				self.canvas.coords(self.current_line, points[-1][0], points[-1][1], self.mouse_p[0], self.mouse_p[1])

		elif self.current_mode == "move_c" or self.current_mode == "component":
			self.current_component[0].move(self.mouse_p, self.current_layout)
			self.draw_layout(self.current_layout)

		elif self.current_mode == "move_w_point":
			self.current_component[0].move_point(self.current_component[1], self.mouse_p)
			self.draw_layout(self.current_layout)

		elif self.current_mode == "move_w_segment":
			self.current_component[0].move_segment(self.current_component[1], self.mouse_p)
			self.draw_layout(self.current_layout)

		elif self.current_mode == "move_brd_pin":
			if self.current_component[0].orientation_brd%2 ==0:
				pos = (self.mouse_p[0], self.current_component[0].brd_pos[1])
			else:
				pos = (self.current_component[0].brd_pos[0], self.mouse_p[1])
			self.current_component[0].move_pin(self.current_component[1], pos, self.current_layout)
			self.draw_layout(self.current_layout)

	def get_close_components(self, point, distance, layout):
		close = []
		for c in self.components:
			dist = LA.norm(np.subtract(point, c.get_pos(layout)))
			if dist < distance:
				close.append((c, 0))
		return close

	def get_closest_component_pin(self, point, distance, layout):
		found = (None, None)
		min_dist = distance
		for c in self.components:
			cpin, pos, min_dist = c.get_close_pin(point, min_dist, layout)
			if cpin is not None:
				found = (c, cpin)
		return found

	def get_close_wire_segment(self, point, distance):
		close = []
		for w in self.wires:
			if not w.has_tag("current"):
				for p0, p1 in zip(w.get_points(), w.get_points()[1:]):
					segment = np.subtract(p1, p0)
					segment_len = LA.norm(segment)
					to_first = np.subtract(p0, point)
					dist = np.fabs(np.cross(to_first, segment))/segment_len
					projected = np.dot(segment, -to_first)/segment_len
					if (dist < distance) and ( -distance < projected < segment_len+distance):
						pi = w.get_points().index(p0)
						close.append((w, pi))
						break
					
		return close

	def get_close_wires_endpoint(self, point, distance):
		close = []
		for w in self.wires:
			if not w.has_tag("current"):
				for p in w.get_pins():
					dist = LA.norm(np.subtract(point, p))
					if dist < distance:
						close.append((w, w.get_pins().index(p)))
						break

		return close

	def get_close_wire_point(self, point, distance):
		close = []
		for w in self.wires:
			if not w.has_tag("current"):
				for p in w.get_points():
					dist = LA.norm(np.subtract(point, p))
					if dist < distance:
						close.append((w, w.get_points().index(p)))
						break

		return close

	def save(self, event):
		filename = filedialog.asksaveasfilename(initialdir="./sch", filetypes =(("sch files","*.sch"),), defaultextension=".sch")
		f = open(filename, "wb")
		if f is None: # asksaveasfile return `None` if dialog closed with "cancel".
			return
		pickle.dump(self.components, f)
		pickle.dump(self.wires, f)

	def load(self, event):
		f = filedialog.askopenfile(mode='rb', initialdir="./sch", filetypes = (("sch files","*.sch"),))
		if f is None:
			return
		self.components = pickle.load(f)
		self.wires = pickle.load(f)
		self.draw_layout("sch")

	def snap_to_grid(self, pos, layout):
		return (round(pos[0]/self.grid)*self.grid, round(pos[1]/self.grid)*self.grid)

root = Tk()
root.geometry("800x500")

app = Window(root)
root.mainloop()
