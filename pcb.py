from PIL import Image, ImageDraw, ImageTk

class Pcb():
	def __init__(self):
		self.wires = []
		self.nets = []
		self.visited_wires = []
		self.changed = True

		self.width = 34
		self.height = 40
		self.strips = [[] for i in range(self.height)]

	def cut_strip(self, row, column):
		pass	

	def update_stripboard(self):
		self.grid = 12
		self.y_start = self.grid
		self.x_start = self.grid*15
		self.strip_h = self.grid-2
		self.hole_d = 6

		self.img = Image.new('RGB', (self.width*self.grid, self.height*self.grid))
		self.draw = ImageDraw.Draw(self.img)
		for y in range(self.height):		#grid 10 10
			self.draw.rectangle([0, y*self.grid, self.width*self.grid, y*self.grid+self.strip_h], fill="darkgoldenrod")
			for x in range(self.width):
				self.draw.ellipse([x*self.grid+self.grid/2-self.hole_d/2, y*self.grid+self.grid/2-self.hole_d/2,
									x*self.grid+self.grid/2+self.hole_d/2, y*self.grid+self.grid/2+self.hole_d/2], fill="black")
		self.stripboard_image = ImageTk.PhotoImage(self.img)

	def draw_stripboard(self, canvas):
		canvas.create_image((self.x_start-self.grid/2, self.y_start-self.grid/2), image=self.stripboard_image, anchor="nw")


	def add_componet(self, component):
		self.components.append(component)

	def get_components(self):
		return self.components

	def reset_nets(self, canvas, components):
		self.create_nets(components)
		self.changed = False

	def create_nets(self, components):
		for component in components:
			for pin_c in component.connections:
				net = set()
				for conn in pin_c:
					net.update(self.GetWireConnections(conn[0]))
				if len(net) > 0: 
					self.nets.append(net)
		self.changed = False

	def get_nets(self):
		return self.nets

	def clear(self):
		self.wires.clear()
		self.nets.clear()
		self.visited_wires.clear()
		self.changed = True

	def GetWireConnections(self, wire):
		if wire.type != "wire":
			exit()
		conn = set()
		wires = []
		if wire in self.visited_wires:
			return conn
		for pin_c in wire.connections:
			for c, p in pin_c:
				if c.type == "wire":
					if c not in self.visited_wires:
						wires.append((c, p))
				else:
					conn.add((c, p))
					for w in c.get_connections(p):	# add all wires connected to this pin
						if w[0] not in self.visited_wires:
							wires.append(w)
		self.visited_wires.append(wire)
		for w in wires:
			conn.update(self.GetWireConnections(w[0]))
		return conn
